## Alternative firmware for Martin Harizanov's 3 channel relay board.
This sketch uses Imroy's fork of pubsubclient, see https://github.com/Imroy/pubsubclient which provides opportunities to use QoS 0, 1 or 2, plus
other features. BUT is not compatible with the original pubsubclient library.  
Commands to control the relays, obtain temperature readings or status information are made via MQTT, and the following topics are hardcoded (although can be easily changed);

### Message topics to the board
Topic | Message | Result
--------------- | ------ | -------------
relayboard/relay1 | on | Relay 1 activated
relayboard/relay2 | on | Relay 2 activated
relayboard/relay3 | on | Relay 3 activated 
relayboard/all | on | All 3 relays activated  
Sending the message **off** will deactivate the respective relay(s).

Topic | Message | Result
--------------- | ------ | -------------
relayboard | status | MQTT response to server  
The response to topic relayboard/status is json encoded in the format;  

![status](/images/status.PNG)

If enabled, the relay board also posts the temperature to the server every 60 seconds(this period can be changed in the sketch);

Topic | Message 
-------------------- | ------
relayboard/temperature | 28.45 

To change the update period, edit `#define timer (60)` in the sketch to whatever value (in seconds).  
If no DS18B20 sensor is fitted, or you wish to disable the function, change the value to 0

Before uploading sketch, ensure 'programming'switch on board is set to PGM!



