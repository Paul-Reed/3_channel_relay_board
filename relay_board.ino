/*
See the readme @ https://gitlab.com/Paul-Reed/3_channel_relay_board/blob/master/README.md

NOTE; This sketch uses Imroy's fork of pubsubclient, see
https://github.com/Imroy/pubsubclient
This will NOT work with Nick O'Leary's pubsubclient library!
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <DallasTemperature.h>

// Update these with values suitable for your system.
const char* mqttUsername = "username";
const char* mqttPassword = "password";
const char *ssid =  "ssid";
const char *pass =  "password";
IPAddress server(192, 168, 1, 8);
unsigned int qosLevel = 2;
// Set the interval (in seconds) for the temperature update frequency.
// If DS18B20 sensor not fitted, or to disable update, set timer to 0
#define timer (0)
#define relayPin1 12
#define relayPin2 13
#define relayPin3 15
#define BUFFER_SIZE 100
int statusF = 0;
float tempC;
unsigned long currentMillis;
unsigned long lastMsg = 0;
String temp_str;

void callback(const MQTT::Publish& pub) {
  String loadstr;
  if (pub.has_stream()) {
    uint8_t buf[BUFFER_SIZE];
    int read;
    while (read = pub.payload_stream()->read(buf, BUFFER_SIZE)) {
      Serial.write(buf, read);
    }
    pub.payload_stream()->stop();
    Serial.println("");
  } else {
    loadstr = pub.payload_string();
    
// Relay 1 control
    if (pub.topic()=="relayboard/relay1") {
      if (loadstr == "on") {
        digitalWrite(relayPin1, HIGH);
      }
      else if (loadstr == "off") {
        digitalWrite(relayPin1, LOW);
      }
    }

// Relay 2 control
    if (pub.topic()=="relayboard/relay2") {    
      if (loadstr == "on") {
        digitalWrite(relayPin2, HIGH);
      }
      else if (loadstr == "off") {
        digitalWrite(relayPin2, LOW);
      }
    }
    
// Relay 3 control
    if (pub.topic()=="relayboard/relay3") {
      if (loadstr == "on") {
        digitalWrite(relayPin3, HIGH);
      }
      else if (loadstr == "off") {
        digitalWrite(relayPin3, LOW);
      }
    }

// Control all 3 relays
    if (pub.topic()=="relayboard/all") {    
      if (loadstr == "on") {
        digitalWrite(relayPin1, HIGH);
        digitalWrite(relayPin2, HIGH);
        digitalWrite(relayPin3, HIGH);
      }
      else if (loadstr == "off") {
        digitalWrite(relayPin1, LOW);
        digitalWrite(relayPin2, LOW);
        digitalWrite(relayPin3, LOW);
      }
    }

// Status report
    if (pub.topic()=="relayboard") {
      if (loadstr == "status") {
        // Set status flag
        statusF = 1;
      }
    }
  }  
}

// Setup Dallas Temperature Sensor
#define ONE_WIRE_BUS 2
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);

WiFiClient wclient;
PubSubClient client(wclient, server);

void setup() {
  // Start up the Dallas Temperature library
  if (timer > 0) {
  sensors.begin();
  }
  pinMode(0, INPUT_PULLUP);
  pinMode(relayPin1, OUTPUT);
  pinMode(relayPin2, OUTPUT);
  pinMode(relayPin3, OUTPUT);
  // Setup console
  Serial.begin(115200);
  delay(10);
  Serial.println();
  Serial.println();
}

void loop() {
  if (WiFi.status() != WL_CONNECTED) {
    Serial.print("Connecting to ");
    Serial.print(ssid);
    Serial.println("...");
    WiFi.begin(ssid, pass);

    if (WiFi.waitForConnectResult() != WL_CONNECTED)
      return;
    Serial.println("WiFi connected");
  }

  if (WiFi.status() == WL_CONNECTED) {
    if (!client.connected()) {
      Serial.println("Connecting to MQTT server");
      if (client.connect(MQTT::Connect("relay_board")
       .set_auth(mqttUsername, mqttPassword))) {
        Serial.println("Connected to MQTT server");
        client.set_callback(callback);
        client.publish(MQTT::Publish("outTopic","hello from relay board")
           .set_qos(qosLevel));
        client.subscribe("relayboard/#");
        } else {
          Serial.println("Could not connect to MQTT server");   
        }
    }
  if (client.connected())
      client.loop();
  }
  
  // Status Request - reply back if flag set
  if (statusF == 1) {
      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& root = jsonBuffer.createObject();
      JsonObject& statusSt = root.createNestedObject("status");
        statusSt.set("relay1", (digitalRead(12)));
        statusSt.set("relay2", (digitalRead(13)));
        statusSt.set("relay3", (digitalRead(15)));
        statusSt.set("rssi", (WiFi.RSSI()));
        if (timer > 0) {
          sensors.requestTemperatures(); // Send the command to get temperatures
          tempC = (sensors.getTempCByIndex(0));
          statusSt.set("temperature", (tempC));
          }
        String statusRes;
        root.printTo(statusRes);
        Serial.println(statusRes);
        client.publish(MQTT::Publish("relayboard/status",statusRes)
           .set_qos(qosLevel));
           
    // Reset the flag
    statusF = 0;
  }

      currentMillis = millis();  //For timer functions
      if ((currentMillis - lastMsg) > (timer*1000)) {
      lastMsg = currentMillis;
         if (timer > 0) {
      sensors.requestTemperatures(); // Send the command to get temperatures
      tempC = (sensors.getTempCByIndex(0));
      temp_str = String(tempC); //converting to a string
      client.publish(MQTT::Publish("relayboard/temperature",temp_str)
           .set_qos(qosLevel));
        }
    }    
}